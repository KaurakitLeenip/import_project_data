import pymongo
import mysql.connector
from pw import passwords
from pull_messages_to_csv import pull_to_csv
import bson
from collections import Counter
import re

CLIENT = pymongo.MongoClient(
    "178.128.117.179",
    username='admin',
    password=passwords['mongodb_pass'])

MYSQL_CLIENT = mysql.connector.connect(
    host='3.0.79.25',
    user='chatbot_dba',
    passwd=passwords['password'],
    database='baania_drupal_online'
)
PROJECT_DATA = CLIENT['project-data-online']['project-data']
PROVINCES = CLIENT['project-data-online']['provinces']
DISTRICTS = CLIENT['project-data-online']['districts']
SUBDISTRICTS = CLIENT['project-data-online']['subdistricts']
BASE = CLIENT['project-data-online']

with open('common_words.txt', 'r') as f:
    COMMON_WORDS = f.readlines()


provinces = None
districts = None
subdistricts = None
QUERY = """
SELECT 
baania_drupal_online.bn_project.title,
baania_drupal_online.bn_project.title_alt,
baania_drupal_online.bn_project.address_project,
baania_drupal_online.view_provinces.name_th,
baania_drupal_online.view_districts.name_th,
baania_drupal_online.view_subdistricts.name_th,
baania_drupal_online.bn_project.nearby,
baania_drupal_online.bn_project.transport

FROM baania_drupal_online.bn_project
join view_districts on bn_project.district_code = view_districts.district_id
join view_provinces on bn_project.province_code = view_provinces.province_id
join view_subdistricts on bn_project.subdistrict_code = view_subdistricts.subdistrict_id;"""

def main():
    # most_recent = PROJECT_DATA.find_one(sort=[("_id", pymongo.DESCENDING)])
    # timestamp = bson.objectid.ObjectId(most_recent['_id']).generation_time.timestamp()
    get_new_projects(0)
    #update_max_values()
    # find_common_words()

def find_common_words():
    c = Counter()
    for project in PROJECT_DATA.find(no_cursor_timeout=True):
        print(project['title'])
        words = project['title'].split()
        c.update(words)
    print(c.most_common(20))

def get_loc_code(loc_type, loc_name):
    LOC_QUERY = """
    select * from baania_drupal_online.view_{0}
    where name_th = {1}
    
    """.format(loc_type, loc_name)
    most_recent = PROJECT_DATA.find_one().sort({'_id': -1})
    timestamp = bson.objectid.ObjectId(most_recent['_id']).generation_time.timestamp()
    get_new_projects(timestamp)

def get_location(table):
    """
    will pull all the locations (province, district and subdistrict)
    from the mysql server into the mongodb server
    :param table: name of the table to pull
    :return: None
    """
    cursor = MYSQL_CLIENT.cursor()
    query = """
    select * from baania_drupal_online.view_{0}
    """.format(table)
    cursor.execute(query)

    for item in cursor:
        BASE[table].insert_one(create_location_record(item, cursor.description))


def create_location_record(loc, headers):
    """
    will create the mongodb document from the mysql query + headers
    and remove uneeded parts of the record
    :param loc: tuple of the record
    :param headers: headers of each filed
    :return: dictionary to be inserted into mongodb
    """
    res = {}
    for i in range(len(loc)):
        res[headers[i][0]] = loc[i]
    res['ngrams'] = ' '.join(construct_ngrams(res['name_th']))
    res['max_score'] = 0
    res.pop('id')
    res.pop('vid')
    res.pop('name_ja')
    return res


def create_location_codes():
    for district in DISTRICTS:
        district['code'] = get_loc_code('districts', district['name'])
        DISTRICTS.save(district)
    for province in PROVINCES:
        province['code'] = get_loc_code('provinces', province['name'])
        PROVINCES.save(province)
    for subdistrict in SUBDISTRICTS:
        subdistrict['code'] = get_loc_code('subdistricts', subdistrict['name'])
        SUBDISTRICTS.save(subdistrict)

def get_new_projects(timestamp):
    """
    given a timestamp pull projects from the mysql database that have been added after
    the timestamp
    :param timestamp: timestamp of most recent mongodb entry
    :return: none
    """
    cursor = MYSQL_CLIENT.cursor()
    cursor.execute(passwords['query'].format(timestamp))
    i = 0
    for project in cursor:
        i += 1
        print(i)
        p = create_new_project(project, cursor.description)
        PROJECT_DATA.insert_one(p)

    #update_max_values()
    print("added " + str(i) + " new projects")


def update_max_values():
    """
    will update the max_score field in the mongodb database by using textsearch on the
    ngrams in the document
    :param collection: mongodb collection to update
    :return: none
    """
    i = 0
    for project in PROJECT_DATA.find({'max_score': 0}, no_cursor_timeout=True):
        searc = project['ngrams']
        if project['numgrams']:
            searc += ' ' + project['numgrams']
#        if project['loc_words']:
#            searc += ' ' + project.get('loc_words')
        i += 1
        print(i)
        res = PROJECT_DATA.find({
            "$text": {
                "$search": searc,
            }
        },
            {
                'name': True,
                "score": {
                    "$meta": "textScore"
                }
            }
        ).sort([("score", {"$meta": "textScore"})]).limit(1)
        try:
            project['max_score'] = res[0]['score']
            PROJECT_DATA.save(project)
        except IndexError:
            print('project {0} could not search'.format(project['title']))
            project['max_score'] = -1
            PROJECT_DATA.save(project)

def create_new_project(project, headers):
    """
    will convert a mysql record to a mongodb document
    :param project: project record
    :param headers: record field names
    :return: dictionary to add to mongodb
    """
    res = {}
    for i in range(len(project)):
        res[headers[i][0]] = project[i]
    trimmed = trim_project_names(res['title'], res)
    res['ngrams'] = ' '.join(get_ngrams(trimmed[0]))
    res['numgrams'] = ' '.join(re.findall(r'\d+', res['title']))
    res['common_words'] = ' '.join(get_ngrams(' '.join(trimmed[1])))
    res['max_score'] = 0.0
    return res


def trim_project_names(name, record):
    """
    will take the common_words.txt file and check if any of those
    words exist within the project name, and remove and return them
    if they exist
    :param name: string name of the project
    :param record: DEPRECATED
    :return: tuple of the resultant name and the removed words
    """
    removed_words = []
    for word in COMMON_WORDS:
        if word in name:
            removed_words.append(word)
            name.replace(word, '')
    return name, removed_words

def get_ngrams(input_str):
    """
    will split a string by spaces and construct ngrams
    of them
    :param input_str: string to create ngrams from
    :return: array containing ngrams of each "word"
    """
    res = []
    for word in input_str.split():
        res.append(' '.join(construct_ngrams(word)))
    return res

def construct_ngrams(input_str):
    """
    will construct ngrams with a minimum length of 2
    :param input_str: input string to create ngrams
    :return: list of ngrams
    """
    if len(input_str) < 3:
        return input_str
    size_range = range(3, max(len(input_str), 2) + 1)
    return list(set(
        input_str[i:i + size]
        for size in size_range
        for i in range(1, max(0, len(input_str) - size) + 1)
    ))

if __name__ == '__main__':
    main()
