import pymongo
import csv
import datetime
from pw import passwords

CLIENT = pymongo.MongoClient(
    "178.128.117.179",
    username='admin',
    password=passwords['mongodb_pass'])
FB_DATA = CLIENT['fb-message-data']['message']

def pull_to_csv():
    with open("june-oct-message.csv", "w", encoding="utf-8") as file:
        headers = ['conversation_id', 'from', 'date', 'text']
        start = datetime.datetime(2018, 6, 1, 0, 0, 0)
        writer = csv.DictWriter(file, fieldnames=headers, lineterminator='\n')
        writer.writeheader()
        temp_id = ''
        thread_id = 0
        for message in FB_DATA.find({'date': {'$gt': start}, 'from': {'$ne': 'Baania.com'}}):
            if message['thread-id'] != temp_id:
                thread_id += 1
                temp_id = message['thread-id']
            writer.writerow(create_dict(message, thread_id))



def create_dict(message, id):
    output = {}
    output['conversation_id'] = id
    output['from'] = message['from']
    output['date'] = message['date']
    output['text'] = message['details']
    return output
