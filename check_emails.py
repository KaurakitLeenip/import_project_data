from imapclient import IMAPClient
from pw import passwords
import email
import pprint
import re
import pymongo

CLIENT = pymongo.MongoClient(
    "178.128.117.179",
    username='admin',
    password=passwords['mongodb_pass'])
IGNORED_USERS = CLIENT['bot_data']['ignored_users']


def check_reply(email_body):
    #print(email_body)
    if "<baaniabot@gmail.com> wrote:" in email_body:
        temp = re.findall(r'USERID:(.*)', email_body)
        return remove_ignored_user(' '.join(temp).strip())
    return False

def remove_ignored_user(id):
    if IGNORED_USERS.find_one({'user_id': id}):
        #IGNORED_USERS.delete_one({'user_id': id})
        return True
    return False

def search_emails(client, folder):
    client.select_folder(folder)
    messages = client.search(['NOT', 'DELETED'])
    response = client.fetch(messages, [b'RFC822', b'ENVELOPE'])
    for msgid, data in response.items():
        print(data[b'ENVELOPE'].in_reply_to)
        parsedEmail = email.message_from_bytes(data[b'RFC822'])

        # print(client.get_flags(msgid))
        # print(client.get_gmail_labels(msgid))
        for part in parsedEmail.get_payload():
            res = check_reply(part.get_payload(decode=True).decode('utf-8'))
            # print(res)
            if res:
                print('--')
                # client.remove_gmail_labels(msgid, client.get_gmail_labels(msgid)[msgid])
                # client.add_flags(msgid, '\\Deleted')
                # print(client.get_flags(msgid))
                # print(client.get_gmail_labels(msgid))

def main():
    with IMAPClient(host="imap.gmail.com") as client:
        client.login("BaaniaBot", passwords['gmail_password'])
        # print(client.list_folders())
        #sent folder name :[Gmail]/Sent Mail
        search_emails(client, "INBOX")
        search_emails(client, "[Gmail]/Sent Mail")

if __name__ == "__main__":
    main()